package com.example.kevin.myapplication.Languages;

import android.graphics.Paint;
import android.text.format.Time;

/**
 * Created by kevin on 5/16/2016.
 */
public abstract class ALanguages {

    public Paint getPaintTextLanguage() {
        return paintTextLanguage;
    }

    public void setPaintTextLanguage(Paint paintTextLanguage) {
        this.paintTextLanguage = paintTextLanguage;
    }

    protected Paint paintTextLanguage;
    public abstract String ConvertStringToLanguage(Time time);

}
