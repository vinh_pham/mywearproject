/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.kevin.myapplication;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.wearable.watchface.CanvasWatchFaceService;
import android.support.wearable.watchface.WatchFaceStyle;
import android.text.format.Time;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.WindowInsets;

import com.example.kevin.myapplication.Languages.ALanguages;
import com.example.kevin.myapplication.Languages.Vietnamese;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.DataApi;
import com.google.android.gms.wearable.DataEvent;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.DataItem;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.DataMapItem;
import com.google.android.gms.wearable.Wearable;

import java.lang.ref.WeakReference;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

/**
 * Digital watch face with seconds. In ambient mode, the seconds aren't displayed. On devices with
 * low-bit ambient mode, the text is drawn without anti-aliasing in ambient mode.
 */
public class MyWatchFace extends CanvasWatchFaceService implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,DataApi.DataListener
{
    private static Typeface NORMAL_TYPEFACE =
            Typeface.create(Typeface.SANS_SERIF, Typeface.NORMAL);
    private static Typeface HOUR_TYPEFACE =
            Typeface.create(Typeface.SANS_SERIF, Typeface.NORMAL);




    /**
     * Update rate in milliseconds for interactive mode. We update once a second since seconds are
     * displayed in interactive mode.
     */
    private static final long INTERACTIVE_UPDATE_RATE_MS = TimeUnit.SECONDS.toMillis(1);

    /**
     * Handler message id for updating the time periodically in interactive mode.
     */
    private static final int MSG_UPDATE_TIME = 0;
    private String StrInforDay = "";
    private String TAG = "BUGWEARSER";
    private GoogleApiClient mGoogleApiClient;

    @Override
    public Engine onCreateEngine() {


        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Wearable.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        mGoogleApiClient.connect();


        /*if ((mGoogleApiClient != null) && mGoogleApiClient.isConnected()) {
            Wearable.DataApi.removeListener(mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }*/

        return new Engine();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.d(TAG, "onConnected(): Successfully connected to Google API client");
        Wearable.DataApi.addListener(mGoogleApiClient, this);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onDataChanged(DataEventBuffer dataEvents) {
        Log.d(TAG, "onDataChanged(): " + dataEvents);

        for (DataEvent event : dataEvents) {
            if (event.getType() == DataEvent.TYPE_CHANGED) {
                String path = event.getDataItem().getUri().getPath();

                if (ListenerDataItemOnService.COUNT_PATH.equals(path)) {
                    Log.d(TAG, "Data Changed for COUNT_PATH");


                    DataItem data = event.getDataItem();
                    DataMap dataMap = DataMapItem.fromDataItem(data).getDataMap();


                    StrInforDay = ""+dataMap.getString("count");

                }
            }
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    private static class EngineHandler extends Handler {
        private final WeakReference<MyWatchFace.Engine> mWeakReference;

        public EngineHandler(MyWatchFace.Engine reference) {
            mWeakReference = new WeakReference<>(reference);
        }

        @Override
        public void handleMessage(Message msg) {
            MyWatchFace.Engine engine = mWeakReference.get();
            if (engine != null) {
                switch (msg.what) {
                    case MSG_UPDATE_TIME:
                        engine.handleUpdateTimeMessage();
                        break;
                }
            }
        }
    }

    private class Engine extends CanvasWatchFaceService.Engine implements DataApi.DataListener {
        final Handler mUpdateTimeHandler = new EngineHandler(this);

        boolean mRegisteredTimeZoneReceiver = false;
        Paint mBackgroundPaint;
        Paint mTextPaintHour;
        boolean mAmbient;
        Time mTime;
        final BroadcastReceiver mTimeZoneReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                mTime.clear(intent.getStringExtra("time-zone"));
                mTime.setToNow();
            }
        };
        int mTapCount;

        float mXOffsetHour;
        float mYOffsetHour;

        /**
         * Whether the display supports fewer bits for each color in ambient mode. When true, we
         * disable anti-aliasing in ambient mode.
         */
        boolean mLowBitAmbient;
        private float mXOffsetMin;
        private float parN;
        private float mYOffsetMin;
        private float parM;
        private float mXOffsetSec;
        private float mYOffsetSec;
        private Paint mTextPaintMin;
        private Paint mTextPaintSec;
        private float textSize;

        private final int DHOUR = 0, DSEC = 2, DMIN = 1, DINFORDAY = 3;
        private Paint mBackgroundHeader;
        private ALanguages Language;
        private float mXOffsetInforDay;
        private float mYOffsetInforDay;

        @Override
        public void onDataChanged(DataEventBuffer dataEventBuffer) {
            Log.d("TAGGING", "Data Changed for COUNT_PATH");
            for (DataEvent event : dataEventBuffer){
                if (event.getType() == DataEvent.TYPE_CHANGED)
                {
                    String path = event.getDataItem().getUri().getPath();
                    if(ListenerDataItemOnService.COUNT_PATH.equals(path))
                    {
                        Log.d("TAGGING", "Data Changed for COUNT_PATH");
                    }
                }
            }
        }

        @Override
        public void onCreate(SurfaceHolder holder) {
            super.onCreate(holder);





            setWatchFaceStyle(new WatchFaceStyle.Builder(MyWatchFace.this)
                    .setCardPeekMode(WatchFaceStyle.PEEK_MODE_VARIABLE)
                    .setBackgroundVisibility(WatchFaceStyle.BACKGROUND_VISIBILITY_INTERRUPTIVE)
                    .setShowSystemUiTime(false)
                    .setAcceptsTapEvents(true)
                    .build());
            Resources resources = MyWatchFace.this.getResources();
            mYOffsetHour = resources.getDimension(R.dimen.digital_y_offset);

            NORMAL_TYPEFACE = Typeface.createFromAsset(getApplicationContext().getAssets(),"fontflag.otf");
            HOUR_TYPEFACE = Typeface.createFromAsset(getApplicationContext().getAssets(),"fontflagmedium.otf");

            Language = new Vietnamese();

            mBackgroundPaint = new Paint();
            mBackgroundPaint.setColor(resources.getColor(R.color.background));

            mBackgroundHeader = new Paint();
            mBackgroundHeader.setColor(resources.getColor(R.color.background_header));

            mTextPaintHour = new Paint();
            mTextPaintHour = createTextPaint(resources,DHOUR);

            mTextPaintMin = new Paint();
            mTextPaintMin = createTextPaint(resources,DMIN);

            mTextPaintSec = new Paint();
            mTextPaintSec = createTextPaint(resources,DSEC);

            Language.setPaintTextLanguage(createTextPaint(resources,DINFORDAY));

            mTime = new Time();
            Log.d("NETWORK","Connected: " + IsNetworkAvaiable());
        }

        private boolean IsNetworkAvaiable() {
            ConnectivityManager manager = (ConnectivityManager) getSystemService(getApplicationContext().CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = manager.getActiveNetworkInfo();
            boolean isAvailable = false;
            if(networkInfo != null && networkInfo.isConnected())
            {
                isAvailable = true;
            }
            isAvailable = false;
            return isAvailable;
        }


        @Override
        public void onDestroy() {
            mUpdateTimeHandler.removeMessages(MSG_UPDATE_TIME);
            super.onDestroy();
        }

        private Paint createTextPaint(Resources resources, int kinds) {
            Paint paint = new Paint();
            switch (kinds){
                case DHOUR:{
                    paint.setTypeface(HOUR_TYPEFACE);
                    paint.setColor(resources.getColor(R.color.color_hour));
                    break;}
                case DMIN:{
                    paint.setTypeface(HOUR_TYPEFACE);
                    paint.setColor(resources.getColor(R.color.color_min));
                    break;}
                case DSEC:{
                    paint.setTypeface(NORMAL_TYPEFACE);
                    paint.setColor(resources.getColor(R.color.color_sec));
                    break;}
                case DINFORDAY:{
                    paint.setTypeface(NORMAL_TYPEFACE);
                    paint.setColor(resources.getColor(R.color.color_sec));
                    break;}
            }

            paint.setAntiAlias(true);
            return paint;
        }

        @Override
        public void onVisibilityChanged(boolean visible) {
            super.onVisibilityChanged(visible);

            if (visible) {
                registerReceiver();

                // Update time zone in case it changed while we weren't visible.
                mTime.clear(TimeZone.getDefault().getID());
                mTime.setToNow();
            } else {
                unregisterReceiver();
            }

            // Whether the timer should be running depends on whether we're visible (as well as
            // whether we're in ambient mode), so we may need to start or stop the timer.
            updateTimer();
        }

        private void registerReceiver() {
            if (mRegisteredTimeZoneReceiver) {
                return;
            }
            mRegisteredTimeZoneReceiver = true;
            IntentFilter filter = new IntentFilter(Intent.ACTION_TIMEZONE_CHANGED);
            MyWatchFace.this.registerReceiver(mTimeZoneReceiver, filter);
        }

        private void unregisterReceiver() {
            if (!mRegisteredTimeZoneReceiver) {
                return;
            }
            mRegisteredTimeZoneReceiver = false;
            MyWatchFace.this.unregisterReceiver(mTimeZoneReceiver);
        }

        @Override
        public void onApplyWindowInsets(WindowInsets insets) {
            super.onApplyWindowInsets(insets);

            // Load resources that have alternate values for round watches.
            Resources resources = MyWatchFace.this.getResources();
            boolean isRound = insets.isRound();
            mXOffsetHour = resources.getDimension(isRound
                    ? R.dimen.digital_x_offset_round : R.dimen.digital_x_offset);
            textSize = resources.getDimension(isRound
                    ? R.dimen.digital_text_size_round : R.dimen.digital_text_size);

            textSize += 5; // tăng đồng loạt sizetext

        }

        private void SolveDimen(float textSize, float centerX, float centerY) {


            parN = textSize;
            parM = textSize;

            mXOffsetMin = centerX + 10;
            mYOffsetMin = mYOffsetHour - parM / 2;

            mXOffsetSec = mXOffsetMin;
            mYOffsetSec = mYOffsetHour;

            mTextPaintMin.setTextSize(textSize / 2);
            mTextPaintSec.setTextSize(textSize / 2);
            Language.getPaintTextLanguage().setTextSize((textSize / 2) - 5 );


        }

        @Override
        public void onPropertiesChanged(Bundle properties) {
            super.onPropertiesChanged(properties);
            mLowBitAmbient = properties.getBoolean(PROPERTY_LOW_BIT_AMBIENT, false);
        }

        @Override
        public void onTimeTick() {
            super.onTimeTick();
            invalidate();
        }

        @Override
        public void onAmbientModeChanged(boolean inAmbientMode) {
            super.onAmbientModeChanged(inAmbientMode);
            if (mAmbient != inAmbientMode) {
                mAmbient = inAmbientMode;
                if (mLowBitAmbient) {
                    mTextPaintHour.setAntiAlias(!inAmbientMode);
                }
                invalidate();
            }

            // Whether the timer should be running depends on whether we're visible (as well as
            // whether we're in ambient mode), so we may need to start or stop the timer.
            updateTimer();
        }

        /**
         * Captures tap event (and tap type) and toggles the background color if the user finishes
         * a tap.
         */
        @Override
        public void onTapCommand(int tapType, int x, int y, long eventTime) {
            Resources resources = MyWatchFace.this.getResources();
            switch (tapType) {
                case TAP_TYPE_TOUCH:
                    // The user has started touching the screen.
                    break;
                case TAP_TYPE_TOUCH_CANCEL:
                    // The user has started a different gesture or otherwise cancelled the tap.
                    break;
                case TAP_TYPE_TAP:
                    // The user has completed the tap gesture.
                    mTapCount++;
                    mBackgroundPaint.setColor(resources.getColor(mTapCount % 2 == 0 ?
                            R.color.background : R.color.background2));
                    break;
            }
            invalidate();
        }

        @Override
        public void onDraw(Canvas canvas, Rect bounds) {
            // Draw the background.
            if (isInAmbientMode()) {
                canvas.drawColor(Color.BLACK);
            } else {
                canvas.drawRect(0, 0, bounds.width(), bounds.height(), mBackgroundPaint);

                canvas.drawRect(0, 0, bounds.width(),  3 * bounds.height() / 5, mBackgroundHeader);
            }

            mTextPaintHour.setTextSize(textSize + 18);

            float centerX = bounds.width() / 2;
            float centerY = bounds.height() / 2;

            if(mTime.hour > 9)
                mXOffsetHour = centerX - mTextPaintHour.getTextSize(); // sizeText chinh la do cao cua text
            else mXOffsetHour = centerX - mTextPaintHour.getTextSize() / 2;
            mYOffsetHour = (bounds.width() / 2f) - 40f;

            SolveDimen(textSize, centerX, centerY);



            // Draw H:MM in ambient mode or H:MM:SS in interactive mode.
            mTime.setToNow();
            /*String text = mAmbient
                    ? String.format("%d:%02d", mTime.hour, mTime.minute)
                    : String.format("%d:%02d:%02d", mTime.hour, mTime.minute, mTime.second);*/

            String stHour = "" + mTime.hour;

            String stMin = "";
            if(mTime.minute < 10)
                stMin = "  ";
            stMin += "" + mTime.minute;
            String stSec ="";
            if(mTime.second < 10)
                stSec = "  ";
            stSec += "" + mTime.second;
/*
            String StrInforDay  = Language.ConvertStringToLanguage(mTime);*/
            if(StrInforDay == "")
                StrInforDay  = "Monday, May 16, 2016";



            mXOffsetInforDay = centerX - StrInforDay.length() * Language.getPaintTextLanguage().getTextSize() / 4 + 10; // textsize chieu cao nen chieu rong / 2 va chi tru 1 nua text de nam giua nen / 2 nua la / 4
            mYOffsetInforDay = centerY + 20;



            canvas.drawText(StrInforDay,mXOffsetInforDay,mYOffsetInforDay,Language.getPaintTextLanguage());
            canvas.drawText(stHour, mXOffsetHour, mYOffsetHour, mTextPaintHour);
            canvas.drawText(stMin, mXOffsetMin, mYOffsetMin, mTextPaintMin);
            canvas.drawText(stSec, mXOffsetSec, mYOffsetSec, mTextPaintSec);


        }

        /**
         * Starts the {@link #mUpdateTimeHandler} timer if it should be running and isn't currently
         * or stops it if it shouldn't be running but currently is.
         */
        private void updateTimer() {
            mUpdateTimeHandler.removeMessages(MSG_UPDATE_TIME);
            if (shouldTimerBeRunning()) {
                mUpdateTimeHandler.sendEmptyMessage(MSG_UPDATE_TIME);
            }
        }

        /**
         * Returns whether the {@link #mUpdateTimeHandler} timer should be running. The timer should
         * only run when we're visible and in interactive mode.
         */
        private boolean shouldTimerBeRunning() {
            return isVisible() && !isInAmbientMode();
        }

        /**
         * Handle updating the time periodically in interactive mode.
         */
        private void handleUpdateTimeMessage() {
            invalidate();
            if (shouldTimerBeRunning()) {
                long timeMs = System.currentTimeMillis();
                long delayMs = INTERACTIVE_UPDATE_RATE_MS
                        - (timeMs % INTERACTIVE_UPDATE_RATE_MS);
                mUpdateTimeHandler.sendEmptyMessageDelayed(MSG_UPDATE_TIME, delayMs);
            }
        }
    }


}
